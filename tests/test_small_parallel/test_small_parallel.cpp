/*********************************************************************************/
/*  Copyright 2022 Ricard Borrell Pol                                            */
/*                                                                               */
/*  This file is part of the GemPa library.                                      */
/*                                                                               */
/*  GemPa is free software: you can redistribute it and/or modify                */
/*  it under the terms of the GNU Lesser General Public License as published by  */
/*  the Free Software Foundation, either version 2.1 of the License, or          */
/*  (at your option) any later version.                                          */
/*                                                                               */
/*  GemPa is distributed in the hope that it will be useful,                     */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*  GNU Lesser General Public License for more details.                          */
/*                                                                               */
/*  You should have received a copy of the GNU Lesser General Public License     */
/*  along with GemPa.  If not, see <https://www.gnu.org/licenses/>.              */
/*********************************************************************************/

#include "Partitioner.h"
#include <iostream>
#include <mpi.h>
#include <fstream>
#include <memory>
#include <vector>

using namespace std;

int main(int argc, char *argv[])
{

  MPI_Init(&argc, &argv);
  int myRank, mySize;
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  MPI_Comm_size(MPI_COMM_WORLD, &mySize);

  gempa::Partitioner P1;

  //
  // Input data
  //
  P1.sdim = 2;
  P1.npart = mySize;
  P1.isParallel = true;
  P1.isWeighted = true;
  P1.doCorr = false;
  P1.printLB = true;
  P1.maxIter = 100;
  P1.targLB = 0.90;

  if (myRank == 0)
  {
    P1.N = 4;

    P1.coords.resize(P1.N * P1.sdim);
    P1.coords[0] = 0.0;
    P1.coords[1] = 0.0;
    P1.coords[2] = 5.0;
    P1.coords[3] = 1.0;
    P1.coords[4] = 3.0;
    P1.coords[5] = 3.0;
    P1.coords[6] = 7.0;
    P1.coords[7] = 3.0;

    P1.weights.resize(P1.N);
    P1.weights[0] = 1;
    P1.weights[1] = 3;
    P1.weights[2] = 2;
    P1.weights[3] = 1;
  }

  else if (myRank == 1)
  {

    P1.N = 5;

    P1.coords.resize(P1.N * P1.sdim);
    P1.coords[0] = 1.0;
    P1.coords[1] = 5.0;
    P1.coords[2] = 5.0;
    P1.coords[3] = 5.0;
    P1.coords[4] = 5.0;
    P1.coords[5] = 7.0;
    P1.coords[6] = 0.0;
    P1.coords[7] = 8.0;
    P1.coords[8] = 8.0;
    P1.coords[9] = 8.0;

    P1.weights.resize(P1.N);
    P1.weights[0] = 1;
    P1.weights[1] = 4;
    P1.weights[2] = 3;
    P1.weights[3] = 1;
    P1.weights[4] = 1;
  }

  else if (myRank == 2)
  {

    P1.N = 2;

    P1.coords.resize(P1.N * P1.sdim);
    P1.coords[0] = 1.0;
    P1.coords[1] = 1.0;
    P1.coords[2] = 7.0;
    P1.coords[3] = 7.0;

    P1.weights.resize(P1.N);
    P1.weights[0] = 10;
    P1.weights[1] = 3;
  }

  vector<int> count;

  if (myRank == 0)
  {
    count.reserve(mySize);
  }

  MPI_Gather(&P1.N, 1, MPI_INT, count.data(), 1, MPI_INT, 0, MPI_COMM_WORLD);

  P1.part();

  vector<int> displacement;

  if (myRank == 0)
  {
    displacement.push_back(0);
    for (int i = 1; i < mySize; i++)
    {
      displacement.push_back(displacement[i - 1] + count[i - 1]);
    }
  }

  int sum = 0;
  MPI_Allreduce(&P1.N, &sum, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

  vector<int> recvbuf;

  if (myRank == 0)
  {
    recvbuf.reserve(sum);
  }

  MPI_Gatherv(P1.partition.data(), P1.N, MPI_INT, recvbuf.data(), count.data(), displacement.data(), MPI_INT, 0, MPI_COMM_WORLD);

  if (myRank == 0)
  {
    ofstream result("partition.txt");
    for (int i = 0; i < sum; i++)
    {
      result << recvbuf[i] << endl;
    }
    result.close();
  }

  MPI_Finalize();
  return 0;
}
