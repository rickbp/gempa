/*********************************************************************************/
/*  Copyright 2022 Ricard Borrell Pol                                            */
/*                                                                               */
/*  This file is part of the GemPa library.                                      */
/*                                                                               */
/*  GemPa is free software: you can redistribute it and/or modify                */
/*  it under the terms of the GNU Lesser General Public License as published by  */
/*  the Free Software Foundation, either version 2.1 of the License, or          */
/*  (at your option) any later version.                                          */
/*                                                                               */
/*  GemPa is distributed in the hope that it will be useful,                     */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*  GNU Lesser General Public License for more details.                          */
/*                                                                               */
/*  You should have received a copy of the GNU Lesser General Public License     */
/*  along with GemPa.  If not, see <https://www.gnu.org/licenses/>.              */
/*********************************************************************************/

#include "Partitioner.h"
#include <iostream>
#include <cmath>
#define use_mpi 1

#if use_mpi
#include <mpi.h>
#endif
#include <algorithm>

using namespace std;

int main(int argc, char *argv[])
{
#if use_mpi
  MPI_Init(&argc, &argv);
#endif
  int myRank, mySize;
#if use_mpi
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  MPI_Comm_size(MPI_COMM_WORLD, &mySize);
#endif

  int mySeed = myRank + 3 + myRank * 15;
  srand(mySeed);

  if (myRank == 0)
    cout << "GeMPa Random test!" << endl;

  gempa::Partitioner P1;

  //
  // Input data
  //
  P1.sdim = 3;
  P1.N = 30000;
  P1.maxIter = 100;
  P1.printLB = true;

  P1.npart = mySize;
  P1.targLB = 0.99;

  P1.isParallel = true;
  P1.isWeighted = true;

  // corr
  P1.doCorr = true;
  P1.corr.resize(P1.npart + 1, 1.0);
  P1.corr[1] = 0.75;
  P1.corr[2] = 1.5;
  P1.corr[3] = 0.75;
  if (!P1.isParallel)
  {
    P1.npart = 3;
  }

  P1.coords.resize(P1.N * P1.sdim);
  P1.weights.resize(P1.N);

  //
  // Fix random coordinates and weights
  //
  for (int i = 0; i < P1.N; ++i)
  {
    int index = i * P1.sdim;
    for (int j = 0; j < P1.sdim; ++j)
    {
      P1.coords[index + j] = pow(((double)rand() / (RAND_MAX)), 2);
    }
    P1.weights[i] = 1 + rand() % 10;
  }

  //
  // Partition and check
  //
  P1.part();

#if use_mpi
  MPI_Finalize();
#endif
  return 0;
}
