/*********************************************************************************/
/*  Copyright 2022 Ricard Borrell Pol                                            */
/*                                                                               */
/*  This file is part of the GemPa library.                                      */
/*                                                                               */
/*  GemPa is free software: you can redistribute it and/or modify                */
/*  it under the terms of the GNU Lesser General Public License as published by  */
/*  the Free Software Foundation, either version 2.1 of the License, or          */
/*  (at your option) any later version.                                          */
/*                                                                               */
/*  GemPa is distributed in the hope that it will be useful,                     */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*  GNU Lesser General Public License for more details.                          */
/*                                                                               */
/*  You should have received a copy of the GNU Lesser General Public License     */
/*  along with GemPa.  If not, see <https://www.gnu.org/licenses/>.              */
/*********************************************************************************/

#include "../include/Partitioner.h"
#include "../include/SFCOrd.h"
#include <cmath>
using namespace std;

#define dbg 0

namespace gempa
{
  Partitioner::Partitioner() : partition(0), coords(0), weights(0), corr(0)
  {
    npart = 0;
    N = 0;
    sdim = 3;
    isWeighted = false;
#if use_mpi
    parComm = MPI_COMM_WORLD;
#endif
    isParallel = true;
    iter = 0;
    maxIter = 1000;
    targLB = -1.0;
    doCorr = false;
    relTol = 1.E-03;
    absTol = 1.E-16;
    maxIdx = pow(pow(2, sdim), 20) - 1;
    printLB = false;
    parSize = 1;
    parRank = 0;
  }

  Partitioner::~Partitioner() {}

  void Partitioner::part()
  {

    begin_part = std::chrono::steady_clock::now();
    if (isParallel)
    {
#if use_mpi
      MPI_Comm_size(parComm, &parSize);
      MPI_Comm_rank(parComm, &parRank);
#endif
    }

    partition.resize(N);
    if (!isWeighted)
    {
      weights.resize(this->N);
      for (int i = 0; i < N; ++i)
        weights[i] = 1;
    }

    evalBboxAndWeights();
    end_Bb = std::chrono::steady_clock::now();
    evalSFCIndexes();
    end_SFC = std::chrono::steady_clock::now();
    histogramIterate();
    end_Hist = std::chrono::steady_clock::now();
  }

  void Partitioner::evalBboxAndWeights()
  {
    int sdim2 = 2 * sdim;
    double locBbox[sdim2 + 1], Bbox[sdim2 + 1]; // last component is for the weight
    double locSumWeight = 0, sumWeight;

    // First iteration
    if (N == 0)
    {
      for (int i = 0; i <= sdim2; ++i)
        locBbox[i] = 0;
    }
    else
    {
      for (int j = 0; j < sdim; ++j)
      {
        locBbox[j] = -coords[j];
        locBbox[j + sdim] = coords[j];
      }
      locBbox[sdim2] = weights[0];
      locSumWeight += weights[0];
    }

    // Rest of iterations
    for (int i = 1; i < N; ++i)
    {
      for (int j = 0; j < sdim; ++j)
      {
        int ind = i * sdim + j;
        if (-locBbox[j] > coords[ind])
          locBbox[j] = -coords[ind];
        if (locBbox[j + sdim] < coords[ind])
          locBbox[j + sdim] = coords[ind];
      }
      if (locBbox[sdim2] < weights[i])
        locBbox[sdim2] = (double)weights[i];
      locSumWeight += (double)weights[i];
    }

    // Parallel MAX and SUM

    if (isParallel)
    {
#if use_mpi
      MPI_Allreduce(&locBbox, &Bbox, sdim2 + 1, MPI_DOUBLE, MPI_MAX, parComm);
      MPI_Allreduce(&locSumWeight, &sumWeight, 1, MPI_DOUBLE, MPI_SUM, parComm);
#endif
    }
    else
    {
      for (int i = 0; i < sdim2 + 1; ++i)
      {
        Bbox[i] = locBbox[i];
      }
      sumWeight = locSumWeight;
    }

    // Final evaluations
    for (int i = 0; i < sdim; ++i)
    {
      minCoord[i] = -Bbox[i];
      maxCoord[i] = Bbox[i + sdim];
      double tolBb = relTol * (maxCoord[i] - minCoord[i] + absTol);
      minCoord[i] -= tolBb;
      maxCoord[i] += tolBb;
    }
    if (!doCorr)
    {
      corr.resize(npart + 1, 1.0);
    }
    double minCorr = npart;
    for (int i = 1; i < corr.size(); ++i)
    {
      if (minCorr > corr[i])
      {
        minCorr = corr[i];
      }
    }
    maxWeight = Bbox[sdim2];
    totWeight = (double)sumWeight;
    avePartWeight = totWeight / npart;
    objLB = (minCorr * avePartWeight) / (minCorr * avePartWeight + maxWeight);
    if (targLB < 0)
      targLB = objLB;
  }

  void Partitioner::evalSFCIndexes()
  {

    sfcIdx.resize(N);
    HilbertSFC sfc;
    sfc.maxLev = log(maxIdx + 1) / (log(pow(2, sdim)));
    sfc.minCoord[0] = minCoord[0];
    sfc.minCoord[1] = minCoord[1];
    sfc.minCoord[2] = minCoord[2];
    sfc.maxCoord[0] = maxCoord[0];
    sfc.maxCoord[1] = maxCoord[1];
    sfc.maxCoord[2] = maxCoord[2];

    for (int i = 0; i < N; ++i)
    {
      sfcIdx[i] = sfc.evalIndex(&coords[sdim * i], sdim);
      partition[i] = ceil(npart * (double)(sfcIdx[i] + 1) / (double)(maxIdx + 1));
    }
  }

  void Partitioner::histogramIterate()
  {

    vector<unsigned long long int> segm(2 * (npart + 1));
    vector<double> segw(2 * (npart + 1));
    vector<double> parwl(npart + 1);
    vector<double> parwg(npart + 1);
    vector<unsigned long long int> part(npart + 1);
    unsigned long long int ind8;

    //
    // Inicialitzar segments
    //
    for (int i = 1; i < npart; ++i)
    {
      int ind = 2 * i;
      segm[ind] = 0;
      segw[ind] = 0.0;
      ind = ind + 1;
      segm[ind] = maxIdx;
      segw[ind] = totWeight;
    }
    part[0] = 0;
    part[npart] = maxIdx + 1;
    double objWeight =
        double(totWeight) /
        double(npart); // rbp: aixo quan hi hagi correccions no servirà!
    //
    // Iterative histogramloop
    //
    for (int l = 0; l < maxIter; ++l)
    {
      for (int i = 0; i <= npart; ++i)
        parwl[i] = 0;
      //    Calculate nous spliting points
      double sumWeight = 0;
      for (int i = 1; i < npart; ++i)
      {
        sumWeight += corr[i] * objWeight;
        part[i] = linear_inverse(segm[2 * i], segw[2 * i], segm[2 * i + 1],
                                 segw[2 * i + 1], sumWeight, objWeight / 2.0);
      }

      // part[k-1]<=partition[i]<part[k]
      for (int i = 0; i < N; ++i)
      {

        ind8 = sfcIdx[i];
        bool next = true;
        // Check bins lower or equal than the previous
        for (int k = partition[i]; k >= 1; --k)
        {
          if (ind8 >= part[k])
            break;
          if (ind8 >= part[k - 1])
          {
            parwl[k] += weights[i];
            partition[i] = k;
            next = false;
            break;
          }
        }

        // Check upper bins
        if (next)
        {
          for (int k = partition[i]; k < npart; ++k)
          {
            if (ind8 < part[k])
              break;
            if (ind8 < part[k + 1])
            {
              parwl[k + 1] += weights[i];
              partition[i] = k + 1;
              break;
            }
          }
        }
      }

      // Reduction
      if (isParallel)
      {
#if use_mpi
        MPI_Allreduce(&(parwl[1]), &(parwg[1]), npart, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif
      }
      else
      {
        for (int i = 1; i <= npart; ++i)
        {
          parwg[i] = parwl[i];
        }
      }
      parwg[0] = 0;

      // Calculate load balance
      double lb = (avePartWeight * corr[1]) / parwg[1];
      for (int i = 1; i < parwg.size(); ++i)
      {
        double auxlb = (avePartWeight * corr[i]) / parwg[i];
        if (auxlb < lb)
          lb = auxlb;
      }
      if (printLB && parRank == 0)
      {
        cout << " SFC Load Balance: " << lb << endl;
      }
      if (lb >= targLB)
        break;
      vector<unsigned long long int> intervals = getIntervals();

      // Evaluate the accumulated weight
      for (int i = 1; i <= npart; ++i)
      {
        parwg[i] += parwg[i - 1];
      }
      // Evaluation of new segments:
      segm[0] = 0;
      segw[0] = 0;
      segm[1] = 0;
      segw[1] = 0;
      int index = 0;
      sumWeight = 0;
      for (int i = 1; i < npart; ++i)
      {
        sumWeight = sumWeight + corr[i] * objWeight;
        int ind = 2 * i;

        if (sumWeight >= segw[ind - 2] && sumWeight <= segw[ind - 1])
        {
          segm[ind] = segm[ind - 2];
          segw[ind] = segw[ind - 2];
          segm[ind + 1] = segm[ind - 1];
          segw[ind + 1] = segw[ind - 1];
        }
        else
        {
          bool larger = true;
          while (index <= npart && larger)
          {
            if (parwg[index] <= sumWeight)
            {
              index = index + 1;
            }
            else
            {
              if (parwg[index - 1] >= segw[ind])
              {
                segm[ind] = part[index - 1];
                segw[ind] = parwg[index - 1];
              }
              if (parwg[index] <= segw[ind + 1])
              {
                segm[ind + 1] = part[index];
                segw[ind + 1] = parwg[index];
              }
              larger = false;
            }
          }
        }
      }
      if (parRank == 0 && false)
      {
        cout << "Obj weight: " << objWeight << endl;
        cout << "Segments: " << endl;
        cout << "Seg1: " << segm[2] << " " << segm[3] << endl;
        cout << "Seg2: " << segm[4] << " " << segm[5] << endl;
        cout << "Segments weight:" << endl;
        cout << "Segw1: " << segw[2] << " " << segw[3] << endl;
        cout << "Segw2: " << segw[4] << " " << segw[5] << endl;
        cout << endl;
      }
      iter = l + 1;
    }
  }

  unsigned long long int Partitioner::linear_inverse(unsigned long long int x1, double y1,
                                                     unsigned long long int x2, double y2,
                                                     double y3, double minWidth)
  {

    double x3f;
    unsigned long long int x3;
    double auxr = x2 - x1;
    if (parRank == 0)
    {
      double x1r = (double)x1;
      double x2r = (double)x2;
    }

    if (y2 - y1 < minWidth)
    {
      x3f = 0.5 * (double)(x2 - x1);
    }
    else
    {
      x3f = auxr * (y3 - y1) / (y2 - y1);
    }

    if (x3f < 0)
    {
      x3 = x1;
    }
    else
    {
      if (x3f > auxr)
      {
        x3 = x2;
      }
      else
      {
        x3 = (unsigned long long int)ceil(x3f);
        x3 = x3 + x1;
      }
    }
    return x3;
  }

  double Partitioner::checkBalance()
  {
    vector<double> parwl(npart + 1, 0.0);
    vector<double> parwg(npart + 1);

    for (int i = 0; i < N; ++i)
    {
      parwl[partition[i]] += weights[i];
    }
    if (isParallel)
    {
#if use_mpi
      MPI_Allreduce(&(parwl[1]), &(parwg[1]), npart, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif
    }
    else
    {
      for (int i = 1; i <= npart; ++i)
      {
        parwg[i] = parwl[i];
      }
    }

    double lb = (avePartWeight * corr[1]) / parwg[1];
    for (int i = 1; i < parwg.size(); ++i)
    {
      double auxlb = (avePartWeight * corr[i]) / parwg[i];
      if (auxlb < lb)
        lb = auxlb;
    }
    return lb;
  }

  vector<unsigned long long int> Partitioner::getIntervals()
  {
    vector<unsigned long long int> minl(npart + 1, maxIdx);
    vector<unsigned long long int> ming(npart + 1);
    vector<unsigned long long int> maxl(npart + 1, 0);
    vector<unsigned long long int> maxg(npart + 1);

    for (int i = 0; i < N; ++i)
    {
      unsigned long long int indx = sfcIdx[i];
      int ipart = partition[i];
      if (indx < minl[ipart])
      {
        minl[ipart] = indx;
      }
      if (indx > maxl[ipart])
      {
        maxl[ipart] = indx;
      }
    }
    if (isParallel)
    {
#if use_mpi
      MPI_Allreduce(&(minl[1]), &(ming[1]), npart, MPI_UNSIGNED_LONG_LONG, MPI_MIN, MPI_COMM_WORLD);
      MPI_Allreduce(&(maxl[1]), &(maxg[1]), npart, MPI_UNSIGNED_LONG_LONG, MPI_MAX, MPI_COMM_WORLD);
#endif
    }
    else
    {
      for (int i = 1; i <= npart; ++i)
      {
        ming[i] = minl[i];
        maxg[i] = maxl[i];
      }
    }

    vector<unsigned long long int> intervals(2 * (npart + 1));
    for (int i = 1; i <= npart; ++i)
    {
      intervals[i * 2] = ming[i];
      intervals[i * 2 + 1] = maxg[i];
    }
    return intervals;
  }

  bool Partitioner::checkOrdering()
  {

    bool ordered = true;
    vector<unsigned long long int> intervals = getIntervals();

    for (int i = 3; i < 2 * (npart + 1); ++i)
    {
      if (intervals[i - 1] > intervals[i])
      {
        ordered = false;
      }
    }
    return ordered;
  }

  void Partitioner::printTimes(int rank) const
  {
    if (parRank == rank)
    {
      cout << "Time Bb:    " << std::chrono::duration_cast<std::chrono::milliseconds>(end_Bb - begin_part).count() << " [ms]" << std::endl;
      cout << "Time SFC:   " << std::chrono::duration_cast<std::chrono::milliseconds>(end_SFC - end_Bb).count() << " [ms]" << std::endl;
      cout << "Time Hist:  " << std::chrono::duration_cast<std::chrono::milliseconds>(end_Hist - end_SFC).count() << " [ms]" << std::endl;
      cout << "TOTAL time: " << std::chrono::duration_cast<std::chrono::milliseconds>(end_Hist - begin_part).count() << " [ms]" << std::endl;
    }
  }

} // namespace gempa
