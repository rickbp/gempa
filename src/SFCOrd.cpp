/*********************************************************************************/
/*  Copyright 2022 Ricard Borrell Pol                                            */
/*                                                                               */
/*  This file is part of the GemPa library.                                      */
/*                                                                               */
/*  GemPa is free software: you can redistribute it and/or modify                */
/*  it under the terms of the GNU Lesser General Public License as published by  */
/*  the Free Software Foundation, either version 2.1 of the License, or          */
/*  (at your option) any later version.                                          */
/*                                                                               */
/*  GemPa is distributed in the hope that it will be useful,                     */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*  GNU Lesser General Public License for more details.                          */
/*                                                                               */
/*  You should have received a copy of the GNU Lesser General Public License     */
/*  along with GemPa.  If not, see <https://www.gnu.org/licenses/>.              */
/*********************************************************************************/

#include "../include/SFCOrd.h"
#include <cmath>
using namespace std;

#define dbg 0

namespace gempa
{

  int typtb2[16] = {1, 0, 0, 2, 0, 1, 1, 3, 3, 2, 2, 0, 2, 3, 3, 1};
  int postc2[16] = {0, 3, 1, 2, 0, 1, 3, 2, 2, 3, 1, 0, 2, 1, 3, 0};
  int postc3[192] = {
      0, 3, 1, 2, 7, 4, 6, 5, 0, 3, 7, 4, 1, 2, 6, 5, 0, 7, 3, 4, 1, 6, 2, 5,
      2, 3, 5, 4, 1, 0, 6, 7, 4, 3, 7, 0, 5, 2, 6, 1, 6, 5, 7, 4, 1, 2, 0, 3,
      0, 7, 1, 6, 3, 4, 2, 5, 0, 1, 7, 6, 3, 2, 4, 5, 2, 5, 1, 6, 3, 4, 0, 7,
      4, 7, 5, 6, 3, 0, 2, 1, 6, 7, 1, 0, 5, 4, 2, 3, 2, 3, 1, 0, 5, 4, 6, 7,
      4, 3, 5, 2, 7, 0, 6, 1, 4, 5, 3, 2, 7, 6, 0, 1, 6, 5, 1, 2, 7, 4, 0, 3,
      6, 1, 5, 2, 7, 0, 4, 3, 0, 1, 3, 2, 7, 6, 4, 5, 2, 1, 3, 0, 5, 6, 4, 7,
      2, 5, 3, 4, 1, 6, 0, 7, 4, 7, 3, 0, 5, 6, 2, 1, 6, 7, 5, 4, 1, 0, 2, 3,
      4, 5, 7, 6, 3, 2, 0, 1, 6, 1, 7, 0, 5, 2, 4, 3, 2, 1, 5, 6, 3, 0, 4, 7};
  int typtb3[192] = {
      1, 6, 6, 11, 11, 12, 12, 14, 0, 2, 2, 3, 3, 4, 4, 5, 16, 1,
      1, 18, 18, 19, 19, 20, 12, 20, 20, 1, 1, 11, 11, 18, 11, 19, 19, 12,
      12, 1, 1, 21, 14, 18, 18, 20, 20, 22, 22, 1, 7, 0, 0, 8, 8, 9,
      9, 10, 6, 16, 16, 23, 23, 21, 21, 22, 21, 14, 14, 6, 6, 23, 23, 11,
      23, 12, 12, 21, 21, 6, 6, 19, 22, 11, 11, 14, 14, 20, 20, 6, 4, 10,
      10, 0, 0, 3, 3, 8, 3, 9, 9, 4, 4, 0, 0, 13, 18, 21, 21, 19,
      19, 16, 16, 12, 5, 8, 8, 10, 10, 15, 15, 0, 20, 23, 23, 22, 22, 14,
      14, 16, 2, 7, 7, 17, 17, 13, 13, 15, 19, 22, 22, 16, 16, 18, 18, 23,
      13, 5, 5, 2, 2, 17, 17, 3, 17, 4, 4, 13, 13, 2, 2, 9, 15, 3,
      3, 5, 5, 10, 10, 2, 8, 13, 13, 9, 9, 7, 7, 4, 10, 17, 17, 15,
      15, 5, 5, 7, 9, 15, 15, 7, 7, 8, 8, 17};

  HilbertSFC::HilbertSFC()
  {
    maxLev = 20;
  }

  HilbertSFC::~HilbertSFC() {}

  unsigned long long int HilbertSFC::evalIndex(double *coord, int sdim) const
  {

    unsigned long long int ind;
    if (sdim == 2)
    {
      ind = evalIndex(coord[0], coord[1]);
    }
    else
    {
      if (sdim != 3)
        return -1;
      ind = evalIndex(coord[0], coord[1], coord[2]);
    }

    return ind;
  }

  unsigned long long int HilbertSFC::evalIndex(double x, double y) const
  {

    unsigned long long int ind = 0;
    unsigned long long int maxIdx = pow(2, maxLev * 2);
    int typ1 = 0;
    double xr, yr;
    unsigned long long int xc, yc, idc, idsfc;

    xr = (x - minCoord[0]) / (maxCoord[0] - minCoord[0]);
    yr = (y - minCoord[1]) / (maxCoord[1] - minCoord[1]);

    for (int ilev = 0; ilev < maxLev; ++ilev)
    {
      maxIdx = maxIdx / 4;
      xc = 0;
      if (xr > 0.5)
      {
        xr = xr - 0.5;
        xc = 1;
      }
      xr *= 2;
      yc = 0;
      if (yr > 0.5)
      {
        yr = yr - 0.5;
        yc = 1;
      }
      yr *= 2;
      idc = 2 * yc + xc;
      idsfc = postc2[typ1 * 4 + idc];
      typ1 = typtb2[typ1 * 4 + idsfc];
      ind = ind + idsfc * maxIdx;
    }
    return ind;
  }

  unsigned long long int HilbertSFC::evalIndex(double x, double y, double z) const
  {

    unsigned long long int ind = 0;
    unsigned long long int maxIdx = pow(2, maxLev * 3);
    int typ1 = 0;
    double xr, yr, zr;
    unsigned long long int xc, yc, zc, idc, idsfc;

    xr = (x - minCoord[0]) / (maxCoord[0] - minCoord[0]);
    yr = (y - minCoord[1]) / (maxCoord[1] - minCoord[1]);
    zr = (z - minCoord[2]) / (maxCoord[2] - minCoord[2]);

    for (int ilev = 0; ilev < maxLev; ++ilev)
    {
      maxIdx = maxIdx / 8;
      xc = 0;
      if (xr > 0.5)
      {
        xr = xr - 0.5;
        xc = 1;
      }
      xr *= 2;
      yc = 0;
      if (yr > 0.5)
      {
        yr = yr - 0.5;
        yc = 1;
      }
      yr *= 2;
      zc = 0;
      if (zr > 0.5)
      {
        zr = zr - 0.5;
        zc = 1;
      }
      zr *= 2;
      idc = 4 * zc + 2 * yc + xc;
      idsfc = postc3[typ1 * 8 + idc];
      typ1 = typtb3[typ1 * 8 + idsfc];
      ind = ind + idsfc * maxIdx;
    }
    return ind;
  }

} // namespace gempa
