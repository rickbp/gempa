# GeMPa

Geometric Mesh Partitioner

**Acknowledgements**

This work was financially supported by the PRACE project funded in part by the EU’s Horizon 2020 Research and Innovation programme (2014-2020) under grant agreement 823767.

**Install**

mkdir build<br>
cd build<br>
cmake ..<br>
make<br>
(make install)<br>

**Test**

cd tests<br>
./compile.py<br>
./execute.py<br>

